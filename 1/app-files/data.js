var APP_DATA = {
  "scenes": [
    {
      "id": "0-19",
      "name": "19",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1728,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948965
      },
      "linkHotspots": [
        {
          "yaw": 0.11737034627503462,
          "pitch": 0.09649221989479883,
          "rotation": 0,
          "target": "3-59"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.5014891421672995,
          "pitch": 0.007157507400158636,
          "title": "Parque Maquilishuat&nbsp;",
          "text": "San Salvador"
        }
      ]
    },
    {
      "id": "1-29",
      "name": "29",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1728,
      "initialViewParameters": {
        "yaw": 0.4413688600151211,
        "pitch": 0.2167376774358818,
        "fov": 1.4115996113023783
      },
      "linkHotspots": [
        {
          "yaw": -3.0499836118669705,
          "pitch": 0.43346812280755387,
          "rotation": 0,
          "target": "3-59"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-39",
      "name": "39",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1728,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948965
      },
      "linkHotspots": [],
      "infoHotspots": []
    },
    {
      "id": "3-59",
      "name": "59",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1728,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948965
      },
      "linkHotspots": [
        {
          "yaw": 2.6002971858168013,
          "pitch": 0.32509566805752854,
          "rotation": 6.283185307179586,
          "target": "0-19"
        },
        {
          "yaw": -0.6495539482571413,
          "pitch": 0.008555149159413133,
          "rotation": 6.283185307179586,
          "target": "1-29"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "parque",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": false,
    "viewControlButtons": true
  }
};
